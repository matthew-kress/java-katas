package com.techelevator;

public class KataRomanNumerals {
	int[] numbers =
		{1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
	String[] romanNumeral =
		{"M","CM","D","CD","C","XC","L","XL","X","IX","V","IV","I"};
	String convert(int num) {
		String roman = "";
		for (int i = 0; i < numbers.length; i++) {
			while (num >= numbers[i]) {
				roman += romanNumeral[i];
				num -= numbers[i];
			}
		}
		return roman;
	}
}
