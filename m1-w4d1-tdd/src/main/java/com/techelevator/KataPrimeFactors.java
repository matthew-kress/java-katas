package com.techelevator;

import java.util.ArrayList;
import java.util.List;

public class KataPrimeFactors {
	public List<Integer> factorize(int n) {
        List<Integer> primes = new ArrayList<Integer>();

        for (int number = 2; n > 1; number++) {
            for (; n % number == 0; n /= number)
                primes.add(number);
        }
        return primes;
    }
}
