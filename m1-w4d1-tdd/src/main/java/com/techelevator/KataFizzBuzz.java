package com.techelevator;

import java.util.ArrayList;
import java.util.List;


	public class KataFizzBuzz {
		public List<String> fizzBuzz(int[] values) {
			List<String> results = new ArrayList<String>();
			
			for(int value : values) {
				if(value > 0 && value <= 100) {
					if(value % 3 == 0 && value % 5 == 0) {
						results.add("FIZZBUZZ");
					}
					else if(value % 3 == 0) {
						results.add("FIZZ");
					} else if(value % 5 == 0) {
						results.add("BUZZ");
					}
					else {
						results.add(Integer.toString(value));
					}
				}
			}
			return results;
		}
	}

