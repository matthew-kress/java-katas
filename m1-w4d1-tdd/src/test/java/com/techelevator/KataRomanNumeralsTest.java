package com.techelevator;

import org.junit.*;
import static org.junit.Assert.*;

public class KataRomanNumeralsTest {
	KataRomanNumerals number;
	String romanNum;
@Before
public void setup() {
	number = new KataRomanNumerals();
}
	
@Test
public void translate_one() {
	romanNum = "I";
	Assert.assertEquals(romanNum, number.convert(1));
}

@Test
public void translate_fifty() {
	romanNum = "L";
	Assert.assertEquals(romanNum, number.convert(50));
}

@Test
public void translate_fiftytwo() {
	romanNum = "LII";
	Assert.assertEquals(romanNum, number.convert(52));
}

@Test
public void translate_hundred() {
	romanNum = "C";
	Assert.assertEquals(romanNum, number.convert(100));
}

@Test
public void translate_hundred_twenty_two() {
	romanNum = "CXXII";
	Assert.assertEquals(romanNum, number.convert(122));
}

@Test
public void translate_thousand() {
	romanNum = "M";
	Assert.assertEquals(romanNum, number.convert(1000));
}

@Test
public void translate_large_number() {
	romanNum = "MMDCLXX";
	Assert.assertEquals(romanNum, number.convert(2670));
}
}
