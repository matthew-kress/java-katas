package com.techelevator;

import org.junit.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

public class KataPrimeFactorsTest {
	private KataPrimeFactors factors;
	
	@Before
	public void setup() {
		factors = new KataPrimeFactors();
	}
	
    @Test
    public void factors_for_two() {
    		List<Integer> result = new ArrayList<Integer>();
    		result.add(2);
        Assert.assertEquals(result, factors.factorize(2));
    }
    
    @Test
    public void factors_for_four() {
    		List<Integer> result = new ArrayList<Integer>();
    		result.add(2);
    		result.add(2);
    		Assert.assertEquals(result, factors.factorize(4));
    }
    
    @Test
    public void factors_for_five() {
    	List<Integer> result = new ArrayList<Integer>();
		result.add(5);
		Assert.assertEquals(result, factors.factorize(5));
    }
    
    @Test
    public void factors_for_ten() {
    	List<Integer> result = new ArrayList<Integer>();
		result.add(2);
		result.add(5);
		Assert.assertEquals(result, factors.factorize(10));
    }
    
    @Test
    public void factors_for_eight() {
     List<Integer> result = new ArrayList<Integer>();
    		result.add(2);
    		result.add(2);
    		result.add(2);
    		Assert.assertEquals(result, factors.factorize(8));
    }

}
