package com.techelevator;

import org.junit.*;
import static org.junit.Assert.*;

import java.util.List;

public class KataFizzBuzzTest {
	private KataFizzBuzz lecture;
	
	@Before
	public void setup() {
		lecture = new KataFizzBuzz();
	}
	
	@Test
	public void given_1_return_1() {
		//Act
		List<String> results = lecture.fizzBuzz(new int[] {1});
		
		//Assert
		Assert.assertEquals(1, results.size());
		Assert.assertEquals("1", results.get(0));
		
	}
	
	@Test
	public void given_12_return_12() {
		//Act
		List<String> results = lecture.fizzBuzz(new int[] {1, 2});
		
		//Assert
		Assert.assertEquals(2, results.size());
		Assert.assertEquals("1", results.get(0));
		Assert.assertEquals("2", results.get(1));
	}
	
	@Test
	public void given_123_return_12Fizz() {
		//Act
		List<String> results = lecture.fizzBuzz(new int[] {1,2,3});
		
		//Assert
		Assert.assertEquals(3, results.size());
		Assert.assertEquals("1", results.get(0));
		Assert.assertEquals("2", results.get(1));
		Assert.assertEquals("FIZZ", results.get(2));
	}
	
	@Test
	public void given_12345_return_12FIZZ4BUZZ() {
		//Act
		List<String> results = lecture.fizzBuzz(new int[] {1,2,3,4,5});
				
		//Assert
		Assert.assertEquals(5, results.size());
		Assert.assertEquals("1", results.get(0));
		Assert.assertEquals("2", results.get(1));
		Assert.assertEquals("FIZZ", results.get(2));
		Assert.assertEquals("4", results.get(3));
		Assert.assertEquals("BUZZ", results.get(4));
	}
	
	@Test
	public void given_9101112131415_return_FIZZBUZZ11FIZZ1314FIZZBUZZ() {
		//Act
		List<String> results = lecture.fizzBuzz(new int[] {9,10,11,12,13,14,15});
						
		//Assert
		Assert.assertEquals(7, results.size());
		Assert.assertEquals("FIZZ", results.get(0));
		Assert.assertEquals("BUZZ", results.get(1));
		Assert.assertEquals("11", results.get(2));
		Assert.assertEquals("FIZZ", results.get(3));
		Assert.assertEquals("13", results.get(4));
		Assert.assertEquals("14", results.get(5));
		Assert.assertEquals("FIZZBUZZ", results.get(6));
	}

}

